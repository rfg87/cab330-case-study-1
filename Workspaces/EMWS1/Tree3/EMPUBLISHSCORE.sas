****************************************************************;
******             DECISION TREE SCORING CODE             ******;
****************************************************************;
 
******         LENGTHS OF NEW CHARACTER VARIABLES         ******;
LENGTH I_ORGYN  $   12;
LENGTH _WARN_  $    4;
 
******              LABELS FOR NEW VARIABLES              ******;
LABEL _NODE_  = 'Node' ;
LABEL _LEAF_  = 'Leaf' ;
LABEL P_ORGYN0  = 'Predicted: ORGYN=0' ;
LABEL P_ORGYN1  = 'Predicted: ORGYN=1' ;
LABEL Q_ORGYN0  = 'Unadjusted P: ORGYN=0' ;
LABEL Q_ORGYN1  = 'Unadjusted P: ORGYN=1' ;
LABEL V_ORGYN0  = 'Validated: ORGYN=0' ;
LABEL V_ORGYN1  = 'Validated: ORGYN=1' ;
LABEL I_ORGYN  = 'Into: ORGYN' ;
LABEL U_ORGYN  = 'Unnormalized Into: ORGYN' ;
LABEL _WARN_  = 'Warnings' ;
 
 
******      TEMPORARY VARIABLES FOR FORMATTED VALUES      ******;
LENGTH _ARBFMT_12 $     12; DROP _ARBFMT_12;
_ARBFMT_12 = ' '; /* Initialize to avoid warning. */
LENGTH _ARBFMT_1 $      1; DROP _ARBFMT_1;
_ARBFMT_1 = ' '; /* Initialize to avoid warning. */
 
 
******             ASSIGN OBSERVATION TO NODE             ******;
IF  NOT MISSING(AGE ) AND
  AGE  <                 44.5 THEN DO;
  _ARBFMT_12 = PUT( REP_AFFL , BEST12.);
   %DMNORMIP( _ARBFMT_12);
  IF _ARBFMT_12 IN ('1' ,'2' ,'3' ,'4' ,'5' ,'6' ,'7' ,'8' ,'9' ,'10' ) THEN
        DO;
    _ARBFMT_1 = PUT( REP_GENDER , $F1.);
     %DMNORMIP( _ARBFMT_1);
    IF _ARBFMT_1 IN ('U' ,'M' ,'.' ) THEN DO;
      _NODE_  =                    9;
      _LEAF_  =                    3;
      P_ORGYN0  =     0.76029567053854;
      P_ORGYN1  =     0.23970432946145;
      Q_ORGYN0  =     0.76029567053854;
      Q_ORGYN1  =     0.23970432946145;
      V_ORGYN0  =     0.78070175438596;
      V_ORGYN1  =     0.21929824561403;
      I_ORGYN  = '0' ;
      U_ORGYN  =                    0;
      END;
    ELSE DO;
      _ARBFMT_12 = PUT( REP_AFFL , BEST12.);
       %DMNORMIP( _ARBFMT_12);
      IF _ARBFMT_12 IN ('1' ,'2' ,'3' ,'4' ,'5' ,'6' ,'7' ) THEN DO;
        _NODE_  =                   16;
        _LEAF_  =                    1;
        P_ORGYN0  =      0.6002994011976;
        P_ORGYN1  =     0.39970059880239;
        Q_ORGYN0  =      0.6002994011976;
        Q_ORGYN1  =     0.39970059880239;
        V_ORGYN0  =     0.55743243243243;
        V_ORGYN1  =     0.44256756756756;
        I_ORGYN  = '0' ;
        U_ORGYN  =                    0;
        END;
      ELSE DO;
        _NODE_  =                   17;
        _LEAF_  =                    2;
        P_ORGYN0  =     0.39477124183006;
        P_ORGYN1  =     0.60522875816993;
        Q_ORGYN0  =     0.39477124183006;
        Q_ORGYN1  =     0.60522875816993;
        V_ORGYN0  =     0.35202492211838;
        V_ORGYN1  =     0.64797507788162;
        I_ORGYN  = '1' ;
        U_ORGYN  =                    1;
        END;
      END;
    END;
  ELSE DO;
    _ARBFMT_1 = PUT( REP_GENDER , $F1.);
     %DMNORMIP( _ARBFMT_1);
    IF _ARBFMT_1 IN ('U' ,'M' ,'.' ) THEN DO;
      _ARBFMT_12 = PUT( REP_AFFL , BEST12.);
       %DMNORMIP( _ARBFMT_12);
      IF _ARBFMT_12 IN ('15' ,'16' ,'17' ,'18' ,'19' ,'20' ,'21' ,'22' ,'23' ,
      '24' ,'25' ,'26' ,'27' ,'28' ,'29' ) THEN DO;
        _NODE_  =                   23;
        _LEAF_  =                    6;
        P_ORGYN0  =      0.1547619047619;
        P_ORGYN1  =     0.84523809523809;
        Q_ORGYN0  =      0.1547619047619;
        Q_ORGYN1  =     0.84523809523809;
        V_ORGYN0  =     0.13953488372093;
        V_ORGYN1  =     0.86046511627906;
        I_ORGYN  = '1' ;
        U_ORGYN  =                    1;
        END;
      ELSE DO;
        _NODE_  =                   22;
        _LEAF_  =                    5;
        P_ORGYN0  =     0.52542372881355;
        P_ORGYN1  =     0.47457627118644;
        Q_ORGYN0  =     0.52542372881355;
        Q_ORGYN1  =     0.47457627118644;
        V_ORGYN0  =     0.59574468085106;
        V_ORGYN1  =     0.40425531914893;
        I_ORGYN  = '0' ;
        U_ORGYN  =                    0;
        END;
      END;
    ELSE DO;
      _NODE_  =                   10;
      _LEAF_  =                    4;
      P_ORGYN0  =     0.19565217391304;
      P_ORGYN1  =     0.80434782608695;
      Q_ORGYN0  =     0.19565217391304;
      Q_ORGYN1  =     0.80434782608695;
      V_ORGYN0  =     0.20149253731343;
      V_ORGYN1  =     0.79850746268656;
      I_ORGYN  = '1' ;
      U_ORGYN  =                    1;
      END;
    END;
  END;
ELSE DO;
  _ARBFMT_12 = PUT( REP_AFFL , BEST12.);
   %DMNORMIP( _ARBFMT_12);
  IF _ARBFMT_12 IN ('13' ,'14' ,'15' ,'16' ,'17' ,'18' ,'19' ,'20' ,'21' ,
  '22' ,'23' ,'24' ,'25' ,'26' ,'27' ,'28' ,'29' ) THEN DO;
    _ARBFMT_12 = PUT( REP_AFFL , BEST12.);
     %DMNORMIP( _ARBFMT_12);
    IF _ARBFMT_12 IN ('16' ,'17' ,'18' ,'19' ,'20' ,'21' ,'22' ,'23' ,'24' ,
    '25' ,'26' ,'27' ,'28' ,'29' ) THEN DO;
      _ARBFMT_12 = PUT( REP_AFFL , BEST12.);
       %DMNORMIP( _ARBFMT_12);
      IF _ARBFMT_12 IN ('20' ,'21' ,'22' ,'23' ,'24' ,'25' ,'26' ,'27' ,'28' ,
      '29' ) THEN DO;
        _NODE_  =                   31;
        _LEAF_  =                   11;
        P_ORGYN0  =                    0;
        P_ORGYN1  =                    1;
        Q_ORGYN0  =                    0;
        Q_ORGYN1  =                    1;
        V_ORGYN0  =               0.0625;
        V_ORGYN1  =               0.9375;
        I_ORGYN  = '1' ;
        U_ORGYN  =                    1;
        END;
      ELSE DO;
        _ARBFMT_1 = PUT( REP_GENDER , $F1.);
         %DMNORMIP( _ARBFMT_1);
        IF _ARBFMT_1 IN ('U' ,'M' ,'.' ) THEN DO;
          _NODE_  =                   54;
          _LEAF_  =                    9;
          P_ORGYN0  =     0.54117647058823;
          P_ORGYN1  =     0.45882352941176;
          Q_ORGYN0  =     0.54117647058823;
          Q_ORGYN1  =     0.45882352941176;
          V_ORGYN0  =     0.71428571428571;
          V_ORGYN1  =     0.28571428571428;
          I_ORGYN  = '0' ;
          U_ORGYN  =                    0;
          END;
        ELSE DO;
          _NODE_  =                   55;
          _LEAF_  =                   10;
          P_ORGYN0  =     0.26119402985074;
          P_ORGYN1  =     0.73880597014925;
          Q_ORGYN0  =     0.26119402985074;
          Q_ORGYN1  =     0.73880597014925;
          V_ORGYN0  =     0.30612244897959;
          V_ORGYN1  =      0.6938775510204;
          I_ORGYN  = '1' ;
          U_ORGYN  =                    1;
          END;
        END;
      END;
    ELSE DO;
      _NODE_  =                   14;
      _LEAF_  =                    8;
      P_ORGYN0  =     0.64004629629629;
      P_ORGYN1  =      0.3599537037037;
      Q_ORGYN0  =     0.64004629629629;
      Q_ORGYN1  =      0.3599537037037;
      V_ORGYN0  =     0.57294429708222;
      V_ORGYN1  =     0.42705570291777;
      I_ORGYN  = '0' ;
      U_ORGYN  =                    0;
      END;
    END;
  ELSE DO;
    _NODE_  =                    6;
    _LEAF_  =                    7;
    P_ORGYN0  =     0.87221803093172;
    P_ORGYN1  =     0.12778196906827;
    Q_ORGYN0  =     0.87221803093172;
    Q_ORGYN1  =     0.12778196906827;
    V_ORGYN0  =     0.87262693156732;
    V_ORGYN1  =     0.12737306843267;
    I_ORGYN  = '0' ;
    U_ORGYN  =                    0;
    END;
  END;
 
****************************************************************;
******          END OF DECISION TREE SCORING CODE         ******;
****************************************************************;
 
drop _LEAF_;
