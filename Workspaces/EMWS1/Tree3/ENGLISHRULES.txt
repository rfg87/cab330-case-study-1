*------------------------------------------------------------*
 Node = 6
*------------------------------------------------------------*
if Replacement: Affluence grade <= 12 or MISSING
AND Age >= 44.5 or MISSING
then 
 Tree Node Identifier   = 6
 Number of Observations = 10604
 Predicted: ORGYN=1 = 0.13
 Predicted: ORGYN=0 = 0.87
 
*------------------------------------------------------------*
 Node = 9
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: U, M, .
AND Replacement: Affluence grade <= 10
AND Age < 44.5
then 
 Tree Node Identifier   = 9
 Number of Observations = 947
 Predicted: ORGYN=1 = 0.24
 Predicted: ORGYN=0 = 0.76
 
*------------------------------------------------------------*
 Node = 10
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: F or MISSING
AND Replacement: Affluence grade >= 11 or MISSING
AND Age < 44.5
then 
 Tree Node Identifier   = 10
 Number of Observations = 1012
 Predicted: ORGYN=1 = 0.80
 Predicted: ORGYN=0 = 0.20
 
*------------------------------------------------------------*
 Node = 14
*------------------------------------------------------------*
if Replacement: Affluence grade <= 15 AND Replacement: Affluence grade >= 13 or MISSING
AND Age >= 44.5 or MISSING
then 
 Tree Node Identifier   = 14
 Number of Observations = 864
 Predicted: ORGYN=1 = 0.36
 Predicted: ORGYN=0 = 0.64
 
*------------------------------------------------------------*
 Node = 16
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: F or MISSING
AND Replacement: Affluence grade <= 7
AND Age < 44.5
then 
 Tree Node Identifier   = 16
 Number of Observations = 668
 Predicted: ORGYN=1 = 0.40
 Predicted: ORGYN=0 = 0.60
 
*------------------------------------------------------------*
 Node = 17
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: F or MISSING
AND Replacement: Affluence grade <= 10 AND Replacement: Affluence grade >= 8 or MISSING
AND Age < 44.5
then 
 Tree Node Identifier   = 17
 Number of Observations = 765
 Predicted: ORGYN=1 = 0.61
 Predicted: ORGYN=0 = 0.39
 
*------------------------------------------------------------*
 Node = 22
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: U, M, .
AND Replacement: Affluence grade <= 14 AND Replacement: Affluence grade >= 11 or MISSING
AND Age < 44.5
then 
 Tree Node Identifier   = 22
 Number of Observations = 354
 Predicted: ORGYN=1 = 0.47
 Predicted: ORGYN=0 = 0.53
 
*------------------------------------------------------------*
 Node = 23
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: U, M, .
AND Replacement: Affluence grade >= 15
AND Age < 44.5
then 
 Tree Node Identifier   = 23
 Number of Observations = 84
 Predicted: ORGYN=1 = 0.85
 Predicted: ORGYN=0 = 0.15
 
*------------------------------------------------------------*
 Node = 31
*------------------------------------------------------------*
if Replacement: Affluence grade >= 20
AND Age >= 44.5 or MISSING
then 
 Tree Node Identifier   = 31
 Number of Observations = 40
 Predicted: ORGYN=1 = 1.00
 Predicted: ORGYN=0 = 0.00
 
*------------------------------------------------------------*
 Node = 54
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: U, M, .
AND Replacement: Affluence grade <= 19 AND Replacement: Affluence grade >= 16 or MISSING
AND Age >= 44.5 or MISSING
then 
 Tree Node Identifier   = 54
 Number of Observations = 85
 Predicted: ORGYN=1 = 0.46
 Predicted: ORGYN=0 = 0.54
 
*------------------------------------------------------------*
 Node = 55
*------------------------------------------------------------*
if Replacement: GENDER IS ONE OF: F or MISSING
AND Replacement: Affluence grade <= 19 AND Replacement: Affluence grade >= 16 or MISSING
AND Age >= 44.5 or MISSING
then 
 Tree Node Identifier   = 55
 Number of Observations = 134
 Predicted: ORGYN=1 = 0.74
 Predicted: ORGYN=0 = 0.26
 
