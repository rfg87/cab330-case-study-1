if ROLE in('INPUT', 'REJECTED') then do;
if upcase(NAME) in(
'IMP_AGE'
'IMP_REP_AFFL'
'REP_GENDER'
) then ROLE='INPUT';
else do;
ROLE='REJECTED';
COMMENT = "Reg2: Rejected using stepwise selection";
end;
end;
