format IMP_REP_AFFL BEST12.0;
label IMP_REP_AFFL = 'Imputed: Replacement: Affluence grade';
IMP_REP_AFFL = REP_AFFL;
if missing(REP_AFFL) then IMP_REP_AFFL = 9;
