if NAME = "AGE" then delete;
else 
if NAME    = "IMP_AGE"  then do;
   ROLE    = "INPUT" ;
   FAMILY  = "" ;
   REPORT  = "N" ;
   LEVEL   = "INTERVAL" ;
   ORDER   = "" ;
end;
else 
if NAME  = "M_AGE " then do;
   ROLE  ="INPUT";
   LEVEL ="BINARY";
 end;
else 
if NAME  = "M_CLASS " then do;
   ROLE  ="INPUT";
   LEVEL ="UNARY";
 end;
if NAME = "LOG_REP_BILL" then delete;
else 
if NAME    = "IMP_LOG_REP_BILL"  then do;
   ROLE    = "INPUT" ;
   FAMILY  = "" ;
   REPORT  = "N" ;
   LEVEL   = "INTERVAL" ;
   ORDER   = "" ;
end;
else 
if NAME  = "M_LOG_REP_BILL " then do;
   ROLE  ="INPUT";
   LEVEL ="BINARY";
 end;
else 
if NAME  = "M_ORGYN " then do;
   ROLE  ="INPUT";
   LEVEL ="UNARY";
 end;
if NAME = "REP_AFFL" then delete;
else 
if NAME    = "IMP_REP_AFFL"  then do;
   ROLE    = "INPUT" ;
   FAMILY  = "" ;
   REPORT  = "N" ;
   LEVEL   = "ORDINAL" ;
   ORDER   = "" ;
end;
else 
if NAME  = "M_REP_AFFL " then do;
   ROLE  ="INPUT";
   LEVEL ="BINARY";
 end;
else 
if NAME  = "M_REP_GENDER " then do;
   ROLE  ="INPUT";
   LEVEL ="UNARY";
 end;
if NAME = "REP_LTIME" then delete;
else 
if NAME    = "IMP_REP_LTIME"  then do;
   ROLE    = "INPUT" ;
   FAMILY  = "" ;
   REPORT  = "N" ;
   LEVEL   = "INTERVAL" ;
   ORDER   = "" ;
end;
else 
if NAME  = "M_REP_LTIME " then do;
   ROLE  ="INPUT";
   LEVEL ="BINARY";
 end;
else 
if NAME  = "M_REP_NEIGHBORHOOD " then do;
   ROLE  ="INPUT";
   LEVEL ="UNARY";
 end;
