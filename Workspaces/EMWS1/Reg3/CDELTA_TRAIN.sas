if ROLE in('INPUT', 'REJECTED') then do;
if upcase(NAME) in(
'IMP_AGE'
'IMP_REP_AFFL'
'IMP_REP_LTIME'
'M_AGE'
'M_LOG_REP_BILL'
'M_REP_AFFL'
'M_REP_LTIME'
'REP_GENDER'
) then ROLE='INPUT';
else do;
ROLE='REJECTED';
COMMENT = "Reg3: Rejected using forward selection";
end;
end;
