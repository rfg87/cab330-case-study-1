*------------------------------------------------------------*;
* Reg3: Create decision matrix;
*------------------------------------------------------------*;
data WORK.ORGYN;
  length   ORGYN                            $  32
           COUNT                                8
           DATAPRIOR                            8
           TRAINPRIOR                           8
           DECPRIOR                             8
           DECISION1                            8
           DECISION2                            8
           ;

  label    COUNT="Level Counts"
           DATAPRIOR="Data Proportions"
           TRAINPRIOR="Training Proportions"
           DECPRIOR="Decision Priors"
           DECISION1="1"
           DECISION2="0"
           ;
  format   COUNT 10.
           ;
ORGYN="1"; COUNT=5505; DATAPRIOR=0.24771632992845; TRAINPRIOR=0.24771632992845; DECPRIOR=.; DECISION1=1; DECISION2=0;
output;
ORGYN="0"; COUNT=16718; DATAPRIOR=0.75228367007154; TRAINPRIOR=0.75228367007154; DECPRIOR=.; DECISION1=0; DECISION2=1;
output;
;
run;
proc datasets lib=work nolist;
modify ORGYN(type=PROFIT label=ORGYN);
label DECISION1= '1';
label DECISION2= '0';
run;
quit;
data EM_DMREG / view=EM_DMREG;
set EMWS1.Impt_TRAIN(keep=
CLASS IMP_AGE IMP_LOG_REP_BILL IMP_REP_AFFL IMP_REP_LTIME M_AGE M_LOG_REP_BILL
M_REP_AFFL M_REP_LTIME ORGYN REP_GENDER REP_NEIGHBORHOOD );
run;
*------------------------------------------------------------* ;
* Reg3: DMDBClass Macro ;
*------------------------------------------------------------* ;
%macro DMDBClass;
    CLASS(ASC) IMP_REP_AFFL(ASC) M_AGE(ASC) M_LOG_REP_BILL(ASC) M_REP_AFFL(ASC)
   M_REP_LTIME(ASC) ORGYN(DESC) REP_GENDER(ASC) REP_NEIGHBORHOOD(ASC)
%mend DMDBClass;
*------------------------------------------------------------* ;
* Reg3: DMDBVar Macro ;
*------------------------------------------------------------* ;
%macro DMDBVar;
    IMP_AGE IMP_LOG_REP_BILL IMP_REP_LTIME
%mend DMDBVar;
*------------------------------------------------------------*;
* Reg3: Create DMDB;
*------------------------------------------------------------*;
proc dmdb batch data=WORK.EM_DMREG
dmdbcat=WORK.Reg3_DMDB
maxlevel = 513
;
class %DMDBClass;
var %DMDBVar;
target
ORGYN
;
run;
quit;
*------------------------------------------------------------*;
* Reg3: Run DMREG procedure;
*------------------------------------------------------------*;
proc dmreg data=EM_DMREG dmdbcat=WORK.Reg3_DMDB
validata = EMWS1.Impt_VALIDATE
outest = EMWS1.Reg3_EMESTIMATE
outterms = EMWS1.Reg3_OUTTERMS
outmap= EMWS1.Reg3_MAPDS namelen=200
;
class
ORGYN
CLASS
IMP_REP_AFFL
M_AGE
M_LOG_REP_BILL
M_REP_AFFL
M_REP_LTIME
REP_GENDER
REP_NEIGHBORHOOD
;
model ORGYN =
CLASS
IMP_AGE
IMP_LOG_REP_BILL
IMP_REP_AFFL
IMP_REP_LTIME
M_AGE
M_LOG_REP_BILL
M_REP_AFFL
M_REP_LTIME
REP_GENDER
REP_NEIGHBORHOOD
/error=binomial link=LOGIT
coding=DEVIATION
nodesignprint
selection=FORWARD choose=VMISC
Hierarchy=CLASS
Rule=NONE
SlEntry=1
SlStay=0.5
Start=0
include=0
MaxStep=0
;
;
code file="C:\CAB330_CS1\DMProj1\Workspaces\EMWS1\Reg3\EMPUBLISHSCORE.sas"
group=Reg3
;
code file="C:\CAB330_CS1\DMProj1\Workspaces\EMWS1\Reg3\EMFLOWSCORE.sas"
group=Reg3
residual
;
run;
quit;
