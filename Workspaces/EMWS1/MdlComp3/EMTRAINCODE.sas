data EMWS1.MdlComp3_EMRANK;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGETLABEL =
   "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg2" MODEL "Reg2" MODELDESCRIPTION "Selection: stepwise" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg2_EMRANK;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMSCOREDIST;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGETLABEL =
   "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg2" MODEL "Reg2" MODELDESCRIPTION "Selection: stepwise" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg2_EMSCOREDIST;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMOUTFIT;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGETLABEL =
   "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg2" MODEL "Reg2" MODELDESCRIPTION "Selection: stepwise" TARGETLABEL "Organics Purchased?";
set WORK.Reg2_OUTFIT;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMCLASSIFICATION;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGETLABEL =
   "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg2" MODEL "Reg2" MODELDESCRIPTION "Selection: stepwise" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg2_EMCLASSIFICATION;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMEVENTREPORT;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGETLABEL =
   "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg2" MODEL "Reg2" MODELDESCRIPTION "Selection: stepwise" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg2_EMEVENTREPORT;
where upcase(TARGET) = upcase("ORGYN");
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg" MODEL "Reg" MODELDESCRIPTION "Selection: none" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg_EMRANK;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMRANK;
set EMWS1.MdlComp3_EMRANK work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg" MODEL "Reg" MODELDESCRIPTION "Selection: none" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg_EMSCOREDIST;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMSCOREDIST;
set EMWS1.MdlComp3_EMSCOREDIST work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg" MODEL "Reg" MODELDESCRIPTION "Selection: none" TARGETLABEL "Organics Purchased?";
set WORK.Reg_OUTFIT;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMOUTFIT;
set EMWS1.MdlComp3_EMOUTFIT work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg" MODEL "Reg" MODELDESCRIPTION "Selection: none" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg_EMCLASSIFICATION;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMCLASSIFICATION;
set EMWS1.MdlComp3_EMCLASSIFICATION work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg" MODEL "Reg" MODELDESCRIPTION "Selection: none" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg_EMEVENTREPORT;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMEVENTREPORT;
set EMWS1.MdlComp3_EMEVENTREPORT work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg3" MODEL "Reg3" MODELDESCRIPTION "Selection: forward" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg3_EMRANK;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMRANK;
set EMWS1.MdlComp3_EMRANK work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg3" MODEL "Reg3" MODELDESCRIPTION "Selection: forward" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg3_EMSCOREDIST;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMSCOREDIST;
set EMWS1.MdlComp3_EMSCOREDIST work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg3" MODEL "Reg3" MODELDESCRIPTION "Selection: forward" TARGETLABEL "Organics Purchased?";
set WORK.Reg3_OUTFIT;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMOUTFIT;
set EMWS1.MdlComp3_EMOUTFIT work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg3" MODEL "Reg3" MODELDESCRIPTION "Selection: forward" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg3_EMCLASSIFICATION;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMCLASSIFICATION;
set EMWS1.MdlComp3_EMCLASSIFICATION work.MdlComp3_TEMP;
run;
data work.MdlComp3_TEMP;
length PARENT $16 MODEL $16 MODELDESCRIPTION $81 DATAROLE $20 TARGET $32 TARGETLABEL $200;
label PARENT = "%sysfunc(sasmsg(sashelp.dmine, rpt_parent_vlabel  ,  NOQUOTE))" MODEL = "%sysfunc(sasmsg(sashelp.dmine, rpt_modelnode_vlabel, NOQUOTE))" MODELDESCRIPTION = "%sysfunc(sasmsg(sashelp.dmine, rpt_modeldesc_vlabel, NOQUOTE))" TARGET =
   "%sysfunc(sasmsg(sashelp.dmine, rpt_targetvar_vlabel, NOQUOTE))" TARGETLABEL = "%sysfunc(sasmsg(sashelp.dmine, meta_targetlabel_vlabel, NOQUOTE))";
retain parent "Reg3" MODEL "Reg3" MODELDESCRIPTION "Selection: forward" TARGETLABEL "Organics Purchased?";
set EMWS1.Reg3_EMEVENTREPORT;
where upcase(TARGET) = upcase("ORGYN");
run;
data EMWS1.MdlComp3_EMEVENTREPORT;
set EMWS1.MdlComp3_EMEVENTREPORT work.MdlComp3_TEMP;
run;
