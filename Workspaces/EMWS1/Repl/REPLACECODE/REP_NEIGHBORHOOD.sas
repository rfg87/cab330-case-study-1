   
* ;
* Defining: REP_NEIGHBORHOOD;
* ;
Length REP_NEIGHBORHOOD$2;
Label REP_NEIGHBORHOOD='Replacement: Type of Residential Neighborhood';
format REP_NEIGHBORHOOD $F2.0;
REP_NEIGHBORHOOD=NEIGHBORHOOD;
* ;
* Variable: NEIGHBORHOOD;
* ;
_UFORMAT200 = strip(
put(NEIGHBORHOOD,$F2.0));
if _UFORMAT200 =  "" then
REP_NEIGHBORHOOD=".";
