   
* ;
* Defining: REP_AFFL;
* ;
Label REP_AFFL='Replacement: Affluence grade';
format REP_AFFL BEST12.0;
REP_AFFL=AFFL;
* ;
* Variable: AFFL;
* ;
_UFORMAT200 = strip(
put(AFFL,BEST12.0));
if _UFORMAT200 =  "0" then
REP_AFFL= .;
else
if _UFORMAT200 =  "31" then
REP_AFFL= .;
else
if _UFORMAT200 =  "34" then
REP_AFFL= .;
