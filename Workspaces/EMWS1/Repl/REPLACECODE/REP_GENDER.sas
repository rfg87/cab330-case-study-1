   
* ;
* Defining: REP_GENDER;
* ;
Length REP_GENDER$1;
Label REP_GENDER='Replacement: GENDER';
format REP_GENDER $F1.0;
REP_GENDER=GENDER;
* ;
* Variable: GENDER;
* ;
_UFORMAT200 = strip(
put(GENDER,$F1.0));
if _UFORMAT200 =  "" then
REP_GENDER=".";
