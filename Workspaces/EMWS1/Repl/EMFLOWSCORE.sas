* ;
* Variable: BILL ;
* ;
Label REP_BILL='Replacement: Total Amount Spent';
REP_BILL =BILL ;
if BILL  eq . then REP_BILL = . ;
else
if BILL <1  then REP_BILL  = . ;
* ;
* Variable: LTIME ;
* ;
Label REP_LTIME='Replacement: Years as Loyalty Card Member';
REP_LTIME =LTIME ;
if LTIME  eq . then REP_LTIME = . ;
else
if LTIME <1  then REP_LTIME  = . ;
   
* ;
* Defining New Variables;
* ;
Label REP_AFFL='Replacement: Affluence grade';
format REP_AFFL BEST12.0;
REP_AFFL= AFFL;
Length REP_GENDER $1;
Label REP_GENDER='Replacement: GENDER';
format REP_GENDER $F1.0;
REP_GENDER= GENDER;
Length REP_NEIGHBORHOOD $2;
Label REP_NEIGHBORHOOD='Replacement: Type of Residential Neighborhood';
format REP_NEIGHBORHOOD $F2.0;
REP_NEIGHBORHOOD= NEIGHBORHOOD;
   
* ;
* Replace Specific Class Levels ;
* ;
length _UFormat200 $200;
drop   _UFORMAT200;
_UFORMAT200 = " ";
* ;
* Variable: AFFL;
* ;
_UFORMAT200 = strip(
put(AFFL,BEST12.0));
if _UFORMAT200 =  "0" then
REP_AFFL= .;
else
if _UFORMAT200 =  "31" then
REP_AFFL= .;
else
if _UFORMAT200 =  "34" then
REP_AFFL= .;
* ;
* Variable: GENDER;
* ;
_UFORMAT200 = strip(
put(GENDER,$F1.0));
if _UFORMAT200 =  "" then
REP_GENDER=".";
* ;
* Variable: NEIGHBORHOOD;
* ;
_UFORMAT200 = strip(
put(NEIGHBORHOOD,$F2.0));
if _UFORMAT200 =  "" then
REP_NEIGHBORHOOD=".";
