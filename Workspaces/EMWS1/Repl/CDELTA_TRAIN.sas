if NAME="BILL" then do;
ROLE="REJECTED";
COMMENT= "Replaced by Repl";
end;
else
if NAME="REP_BILL" then do;
ROLE="INPUT";
LEVEL="INTERVAL";
end;
else
if NAME="LTIME" then do;
ROLE="REJECTED";
COMMENT= "Replaced by Repl";
end;
else
if NAME="REP_LTIME" then do;
ROLE="INPUT";
LEVEL="INTERVAL";
end;
if NAME="AFFL" then ROLE="REJECTED";
else
if NAME="REP_AFFL" then do;
ROLE="INPUT";
LEVEL="ORDINAL";
end;
else
if NAME="GENDER" then ROLE="REJECTED";
else
if NAME="REP_GENDER" then do;
ROLE="INPUT";
LEVEL="NOMINAL";
end;
else
if NAME="NEIGHBORHOOD" then ROLE="REJECTED";
else
if NAME="REP_NEIGHBORHOOD" then do;
ROLE="INPUT";
LEVEL="ORDINAL";
end;
